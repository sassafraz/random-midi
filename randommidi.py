#!/usr/bin/env python3.7
from midiutil.MidiFile import MIDIFile
import random

mf = MIDIFile(1)     # only 1 track
track = 0   
time = 0    # start at the beginning
mf.addTrackName(track, time, "Sample Track")
mf.addTempo(track, time, random.randint(100, 250))

channel = 0
volume = 100

# C4 (middle C)
randdur = 0
hmm = random.randint(5, 17)
for i in range(hmm):
        pitch = random.randint(10, 160)
        time = randdur+1
        randdur = random.randint(1, 8)
        duration = randdur
        mf.addNote(track, channel, pitch, time, duration, volume)

# add more voices
if(random.randint(0, 100) % 2 == 0):
        randdur = 0
        for i in range(random.randint(1, 10)):
                for i in range(hmm):
                        pitch = random.randint(10, 160)
                        time = randdur+1
                        randdur = random.randint(1, 8)
                        duration = randdur
                        mf.addNote(track, channel, pitch, time, duration, volume)

with open("multioutput.mid", 'wb') as outf:
    mf.writeFile(outf)